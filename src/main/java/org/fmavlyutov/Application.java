package org.fmavlyutov;

import java.util.Scanner;

import static org.fmavlyutov.constant.CommandLineConstant.*;
import static org.fmavlyutov.constant.CommandLineConstant.EXIT;

public class Application {

    public static void main(String[] args) {
        displayHello();
        displayArgs(args);
        run();
    }

    private static void displayArgs(String[] args) {
        if (args == null || args.length == 0) {
            return;
        }
        for (String arg : args) {
            switch (arg) {
                case HELP:
                    displayHelp();
                    break;
                case VERSION:
                    displayVersion();
                    break;
                case ABOUT:
                    displayAbout();
                    break;
                default:
                    displayError();
            }
        }
    }

    private static void run() {
        String arg = "";
        Scanner scanner = new Scanner(System.in);
        while (!arg.equals(EXIT)) {
            arg = scanner.next();
            displayArg(arg);
        }
    }

    private static void displayArg(String arg) {
        if (arg == null || arg.isEmpty()) {
            return;
        }
        switch (arg) {
            case HELP:
                displayHelp();
                break;
            case VERSION:
                displayVersion();
                break;
            case ABOUT:
                displayAbout();
                break;
            case EXIT:
                exit();
                break;
            default:
                displayError();
        }
    }

    private static void displayHello() {
        System.out.println("-----HELLO-----\n");
    }

    private static void displayGoodbye() {
        System.out.println("-----GOODBYE-----");
    }

    private static void displayHelp() {
        System.out.printf("[%s] - display command line arguments\n", HELP);
        System.out.printf("[%s] - display program version\n", VERSION);
        System.out.printf("[%s] - display info about author\n", ABOUT);
        System.out.printf("[%s] - finish program\n", EXIT);
        System.out.println();
    }

    private static void displayVersion() {
        System.out.println("Version: 1.0.0\n");
    }

    private static void displayAbout() {
        System.out.println("Author: Philip Mavlyutov\n");
    }

    private static void exit() {
        displayGoodbye();
        System.exit(0);
    }

    private static void displayError() {
        System.out.printf("Incorrect argument. Enter [%s] to see all arguments\n", HELP);
        System.out.println();
    }

}
